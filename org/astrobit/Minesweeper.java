package org.astrobit;

import java.util.Random;
import java.util.Scanner;

public class Minesweeper {
    private static final int WIDTH = 10;
    private static final int HEIGHT = 10;
    private static final char[][] grid = new char[HEIGHT][WIDTH];

    private static final boolean[][] revealed = new boolean[HEIGHT][WIDTH]; // Tracks revealed cells


    private static final int NUM_MINES = 10;


    public static void main(String[] args) {
        initializeGrid();
        playGame();
    }

    private static void playGame() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter row and column numbers (e.g., 3 5):");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            if (row < 0 || row >= HEIGHT || col < 0 || col >= WIDTH) {
                System.out.println("Invalid input. Try again.");
                continue;
            }
            if (grid[row][col] == '*') {
                System.out.println("Game Over! You hit a mine.");
                revealAllMines();
                printGrid();
                break;
            } else if (!revealed[row][col]) {
                revealed[row][col] = true;
                if (checkWinCondition()) {
                    System.out.println("Congratulations! You have cleared all the mines!");
                    revealAllCells();
                    printGrid();
                    break;
                }
            }
            printGrid();
        }
    }

    private static boolean checkWinCondition() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                if (grid[i][j] != '*' && !revealed[i][j]) {
                    return false; // Not all non-mine cells are revealed
                }
            }
        }
        return true; // All non-mine cells are revealed
    }

    private static void revealAllMines() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                if (grid[i][j] == '*') {
                    revealed[i][j] = true;
                }
            }
        }
    }

    private static void revealAllCells() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                revealed[i][j] = true;
            }
        }
    }

    private static void calculateClues() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                if (grid[i][j] == '*') continue; // Skip mines
                int mineCount = 0;
                // Check all adjacent cells
                for (int di = -1; di <= 1; di++) {
                    for (int dj = -1; dj <= 1; dj++) {
                        int ni = i + di, nj = j + dj;
                        if (ni >= 0 && ni < HEIGHT && nj >= 0 && nj < WIDTH && grid[ni][nj] == '*') {
                            mineCount++;
                        }
                    }
                }
                grid[i][j] = (char) ('0' + mineCount); // Convert number to character
            }
        }
    }

    private static void initializeGrid() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                grid[i][j] = '-';
                revealed[i][j] = false; // Initialize all cells as unrevealed
            }
        }
        placeMines();
        calculateClues();
    }

    private static void printGrid() {
        System.out.print("   "); // Spacing for row numbers
        for (int j = 0; j < WIDTH; j++) {
            System.out.print(j + " "); // Print column numbers
        }
        System.out.println();

        for (int i = 0; i < HEIGHT; i++) {
            System.out.print(i + "  "); // Print row number
            for (int j = 0; j < WIDTH; j++) {
                if (revealed[i][j]) {
                    System.out.print(grid[i][j] + " ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }

    private static void placeMines() {
        Random random = new Random();
        int placedMines = 0;
        while (placedMines < NUM_MINES) {
            int row = random.nextInt(HEIGHT);
            int col = random.nextInt(WIDTH);
            if (grid[row][col] != '*') {
                grid[row][col] = '*';
                placedMines++;
            }
        }
    }
}
