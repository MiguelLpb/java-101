# java-101



## A small Minesweeper game in java

This is a basic Minesweeper game based on Microsoft's version from the 1990s.
Basic mechanics have been implemented. Tests are coming soon (I promise)

![img.png](img.png)
